//
//  LocalizationView.swift
//  Doggy
//
//  Created by mac on 27/5/15.
//  Copyright (c) 2015 mac. All rights reserved.
//


import UIKit
extension UILabel{
    var localizedText: String{
        set(key){
            text = NSLocalizedString(key, comment: "")
        }
        get{
            return text!
        }
    }
}
extension UINavigationItem{
    var localizedText : String{
        set(key){
            self.title = NSLocalizedString(key, comment: "")
        }
        get{
            return title!
        }
    }
}
@IBDesignable class LocalizeNavigationItem : UINavigationItem{
    
   @IBInspectable  var localizeTitle: String = "" {
        didSet{
            #if TARGET_INTERFACE_BUILDER
            var bundle = NSBundle(forClass: self.dynamicType)
            self.title = bundle.localizedStringForKey(self.localizeTitle, value: "", table: nil)
            #else
                self.title = NSLocalizedString(self.localizeTitle, comment: "" )
            #endif
        }
    
    }
}
extension UIButton{
    var localizedTitleNormal:String{
        
        set(key){
            setTitle(NSLocalizedString(key, comment: ""), forState: .Normal)
        }
        get{
            return titleForState(.Normal)!
        }
    }
    var localizedTitleHighlighted:String{
        set(key){
            setTitle(NSLocalizedString(key, comment: ""), forState: .Normal)
        }
        get{
            return titleForState(.Highlighted)!
        }
    }
}