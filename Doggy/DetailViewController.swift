//
//  DetailViewController.swift
//  Doggy
//
//  Created by mac on 26/5/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController{
    

    @IBOutlet weak var descriptionLabel: UILabel!
    
    var detailItem: AnyObject?{
        didSet{
            self.configureView()
        }
    }
    
    func configureView(){
        
        if let detail: AnyObject = self.detailItem{
            if let label = self.descriptionLabel{
                label.text = detail.valueForKey("timeStamp")!.description
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.configureView()
    }
    
    

}