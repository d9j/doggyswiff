//
//  R.swift
//  Doggy
//
//  Created by mac on 31/5/15.
//  Copyright (c) 2015 mac. All rights reserved.
//


struct  R {
    struct Strings {
       static let alertNewName = "alert.new_name".local
       static let alertMessage = "alert.message".local
       static let alertSave = "alert.save".local
       static let alertCancel = "alert.cancel".local
    }
    
    struct Id {
        static let cellId = "Cell"
    }
}