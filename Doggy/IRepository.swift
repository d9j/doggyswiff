//
//  IRepository.swift
//  Doggy
//
//  Created by mac on 1/6/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

protocol iRepository{
    
    
    func get(mindex: Int)
    
    func count()->Int
    
    func add(name: String)
}
