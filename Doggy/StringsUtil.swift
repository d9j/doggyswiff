//
//  StringsUtil.swift
//  Doggy
//
//  Created by mac on 28/5/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

import Foundation


extension String{
    
    var isBlank: Bool{
        
        get{
            let trimmed = stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            return trimmed.isEmpty
        }
    }
    var local : String{
        get{
            let translated = NSLocalizedString(self, tableName: nil, comment: "")
            return translated
        }
        
    }
}
