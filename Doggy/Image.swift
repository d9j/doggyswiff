//
//  Image.swift
//  Doggy
//
//  Created by mac on 23/5/15.
//  Copyright (c) 2015 mac. All rights reserved.
//


class Image : Imageable {
   
    let url : String
    let hash : String
    
    init(url: String, hash: String){
        self.url = url
        self.hash = hash
    }
    
}
