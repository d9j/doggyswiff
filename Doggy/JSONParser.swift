//
//  JsonParser.swift
//  Doggy
//
//  Created by mac on 25/5/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

import Foundation


struct JSONParser{
    
    static func parseImages(data: NSData?) -> [Image]{
        return self.dictionariesFromData(data)?.map(self.parseImage) ?? []
    }
    static func parseImagesArray(data: NSData?) -> [Image]{
        if let data = data{
            if let array = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: nil) as? [NSDictionary]{
                return array.map(self.parseImage) ?? []
            }
        }
        return []
    }
    private static func parseImage(image: NSDictionary) -> Image{
        let hash = image["hash"] as? String ?? ""
        let url = image["url"] as? String ?? ""
        return Image(url: url, hash: hash)
    }
    private static func dictionariesFromData(data: NSData?)-> [NSDictionary]?{
        if let data = data{
            if let dictionary = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: nil) as? NSDictionary{
                if let items = dictionary["images"] as? [NSDictionary]{
                    return items
                }
                
            }
        }
        return nil;
    }
}