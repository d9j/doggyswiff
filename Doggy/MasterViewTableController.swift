//
//  MasterViewTableController.swift
//  Doggy
//
//  Created by mac on 25/5/15.
//  Copyright (c) 2015 mac. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class MasterViewTableController : UITableViewController,NSFetchedResultsControllerDelegate,UITableViewDataSource{
    var people = [NSManagedObject]()

    
    @IBOutlet weak var mTableView: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: R.Id.cellId)
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "insertNewObject:")
        self.navigationItem.rightBarButtonItem = addButton
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return people.count
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell  {
        let cellId = R.Id.cellId
        let person = people[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier(cellId,forIndexPath: indexPath ) as! UITableViewCell
    
        cell.textLabel!.text = person.valueForKey("name") as? String
        
        return cell
    }
    
    func insertNewObject(sender: AnyObject){
        var title =  R.Strings.alertNewName
        var message = R.Strings.alertMessage
        var alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let saveAction = UIAlertAction(title: R.Strings.alertSave,style: .Default, handler:
            {
            (action: UIAlertAction!) -> Void in
            let textField = alertController.textFields![0] as! UITextField
                
        
            self.tableView.reloadData()
            })
        let cancelAction = UIAlertAction(title: R.Strings.alertCancel,style: .Default, handler:
            {
                (action: UIAlertAction!) -> Void in
            })
        alertController.addTextFieldWithConfigurationHandler({(textField: UITextField!) -> Void in})
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
     } // insertNewObject
    
    


        
}
    



