//
//  PeopleRepository.swift
//  Doggy
//
//  Created by mac on 31/5/15.
//  Copyright (c) 2015 mac. All rights reserved.
//


import CoreData
import UIKit
class PeopleRepository  {
    
    
    var people = [NSManagedObject]()
    
    
    func count()->Int{
        return people.count
    }
    func get(nindex: Int )->NSManagedObject{
      return  people[nindex]
    }
    func add(name: String){
        //1
      let appDelegate =   UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        //2
        let entity = NSEntityDescription.entityForName("Person", inManagedObjectContext: managedContext)
        let person = NSManagedObject(entity:  entity!, insertIntoManagedObjectContext: managedContext)
        //3
        person.setValue(name, forKey: "name")
        //4
        var error : NSError?
        
        if !managedContext.save(&error){
            println("Couldn't save Object \(error) , \(error?.userInfo))")
        }
        self.people.append(person)
    }
    
}



